let cardjs = "./widget/card/card.js";
let genericInputjs = "./widget/input/normal-input.js";
let genericRadioCardjs = "./widget/input/radio-card/generic-radio-wrapper.js";
let sliderjs = "./widget/input/slider/slider.js";
let buttonjs = "./widget/button/button.js";
let infojs = "./widget/info/info.js";
let stepsWrapperJs = "./widget/steps-wrapper/steps-wrapper.js";
let finalReportWrapperJs = "./widget/finalreport/finalreportwrapper.js";
let step1Js = "./widget/steps-wrapper/step1.js";
let step2Js = "./widget/steps-wrapper/step2.js";
let shareWrapperJs = "./widget/share/share-wrapper.js";

let step = 1;
let params = {
  reference_id: 0
};
let calculatedData = {};
let ulipCalculatorApiUrl = "https://beta.utimf.com/calculator/ulip-calculator/";

// fetching element where calculator widget need to be attached
let calculatorblock = document.getElementById("calculator");
calculatorblock.classList.add("utimf-fdhfdkjfkaljw378yfcnca");

function addStyleSheet() {
  let styleSheet = "./index.css";
  let link = document.createElement("link");
  let head = document.getElementsByTagName("head");
  head[0].append(link);
  props = {
    href: styleSheet,
    rel: "stylesheet",
    type: "text/css"
  };
  setAttr(link, props);
}

function UlipApiCall(params) {
  document.getElementById("container-wrapper").classList.add("show-loader");
  let xhttp = new XMLHttpRequest();
  xhttp.addEventListener("error", e => {
    console.log("Some error occured!");
    document
      .getElementById("container-wrapper")
      .classList.remove("show-loader");
  });
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if (this.responseText !== "") {
        calculatedData = JSON.parse(this.responseText);
        params.reference_id = calculatedData.reference_id;
        // functionality for updating dom to info of next view
        clearDivison("step4CardWrapper");
        render((step = 4));
        document
          .getElementById("container-wrapper")
          .classList.remove("show-loader");
      }
    }
  };
  xhttp.open("POST", ulipCalculatorApiUrl, true);
  xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send(JSON.stringify(params));
}

function hide(id) {
  document.getElementById(id).style.display = "none";
}
function show(id) {
  if (id == "step1NextBtn" || id == "step1SaveBtn" || id == "step2-saveBtn") {
    document.getElementById(id).style.display = "flex";
  } else {
    document.getElementById(id).style.display = "block";
  }
}

function setAttr(el, props) {
  Object.keys(props).map(key => {
    el.setAttribute(key, props[key]);
  });
}

let getComponent = (component, props) => {
  return import(component).then(function(module) {
    return module.default(props);
  });
};

function stepsWrapper() {
  let stepsWrapperProps = {
    appendAtId: "calculator",
    params: params,
    func: render
  };

  let stepsWrapper = getComponent(stepsWrapperJs, stepsWrapperProps);
}

function clearDivison(id) {
  let node = document.getElementById(id);
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}
function render() {
  switch (step) {
    case 1:
      stepOneCardRender();
      break;
    case 2:
      stepTwoCardRender();
      break;
    case 3:
      stepThreeCardRender();
      break;
    case 4:
      stepFourCardRender();
      break;
    default:
      defaultStepRender();
  }
}

function stepOneCardRender() {
  function validateForm(
    fields = {
      name: document.getElementById("name").value,
      email_id: document.getElementById("email").value,
      mobile_number: parseInt(document.getElementById("phone").value),
      gender: document.getElementById("genderValue").innerText,
      date_of_birth: document.getElementById("age").value
    }
  ) {
    if (
      fields.name &&
      fields.email_id &&
      fields.mobile_number &&
      fields.gender &&
      fields.date_of_birth &&
      document.getElementById("name-error").innerText == "" &&
      document.getElementById("email-error").innerText == "" &&
      document.getElementById("phone-error").innerText == "" &&
      document.getElementById("age-error").innerText == ""
    ) {
      let dob = new Date(fields.date_of_birth);
      fields.dob = dob;
      fields.date_of_birth = `${("0" + dob.getDate()).slice(-2)}/${(
        "0" +
        (dob.getMonth() + 1)
      ).slice(-2)}/${dob.getFullYear()}`;

      Object.keys(fields).map(e => {
        params[e] = fields[e];
      });

      // functionality for updating dom to info of first view
      hide("step1Card");

      // emptying card 2's wrapper
      let step2CardWrapper = document.getElementById("step2CardWrapper");
      while (step2CardWrapper.firstChild) {
        step2CardWrapper.removeChild(step2CardWrapper.firstChild);
      }
      prevStep = 1;
      step = 2;
      render((step = 2));

      //Rendering info for step 1
      let props1 = {
        appendAtId: "step1InfoWrapper",
        infoObject: {
          Name: params.name,
          DOB: `${params.date_of_birth}`,
          Gender: params.gender,
          "Mobile number": params.mobile_number,
          "Email Id": params.email_id
        },
        editButtonclassList: ["editIcon", "step1EditButton"],
        inputId: "step1Edit"
      };

      document
        .getElementById("step1Wrapper")
        .classList.remove("borderInactive");
      document.getElementById("step1Wrapper").classList.add("borderActive");

      document.getElementById("step1Icon").classList.remove("activeIcon");
      document.getElementById("step1Icon").classList.add("filledIcon");

      document
        .getElementById(`step${step}Icon`)
        .classList.remove("inactiveIcon");
      document.getElementById(`step${step}Icon`).classList.remove("filledIcon");

      document.getElementById(`step${step}Icon`).classList.add("activeIcon");

      getComponent(step1Js, props1);
      console.log("You are redirected to next page");
    }
  }
  function saveForm(
    fields = {
      name: document.getElementById("name").value,
      email_id: document.getElementById("email").value,
      mobile_number: parseInt(document.getElementById("phone").value),
      gender: document.getElementById("genderValue").innerText,
      date_of_birth: document.getElementById("age").value
    }
  ) {
    prevStep = 1;
    step = 2;

    hide("step1Card");
    if (
      fields.name &&
      fields.email_id &&
      fields.mobile_number &&
      fields.gender &&
      fields.date_of_birth &&
      document.getElementById("name-error").innerText == "" &&
      document.getElementById("email-error").innerText == "" &&
      document.getElementById("phone-error").innerText == "" &&
      document.getElementById("age-error").innerText == ""
    ) {
      let dob = new Date(fields.date_of_birth);
      fields.dob = dob;
      fields.date_of_birth = `${("0" + dob.getDate()).slice(-2)}/${(
        "0" +
        (dob.getMonth() + 1)
      ).slice(-2)}/${dob.getFullYear()}`;

      Object.keys(fields).map(e => {
        params[e] = fields[e];
      });
    }

    // rendering info for step 1
    let props1 = {
      appendAtId: "step1InfoWrapper",
      infoObject: {
        Name: params.name,
        DOB: params.date_of_birth,
        Gender: params.gender,
        "Mobile number": params.mobile_number,
        "Email Id": params.email_id
      },
      editButtonclassList: ["editIcon", "step1EditButton"],
      inputId: "step1Edit"
    };
    getComponent(step1Js, props1);
    // emptying card 3's wrapper
    let step3CardWrapper = document.getElementById("step3CardWrapper");
    while (step3CardWrapper.firstChild) {
      step3CardWrapper.removeChild(step3CardWrapper.firstChild);
    }

    let step2InfoWrapper = document.getElementById("step2InfoWrapper");
    while (step2InfoWrapper.firstChild) {
      step2InfoWrapper.removeChild(step2InfoWrapper.firstChild);
    }

    document.getElementById("step1Icon").classList.remove("activeIcon");
    document.getElementById("step1Icon").classList.add("filledIcon");

    document.getElementById(`step${step}Icon`).classList.remove("filledIcon");
    document.getElementById(`step${step}Icon`).classList.add("activeIcon");

    show("step2Card");
  }
  let cardProps = {
    appendAtId: "step1CardWrapper",
    inputId: "step1Card"
  };
  let NameProps = {
    inputId: "name",
    label: "Name:",
    type: "text",
    appendAtId: "step1Card-inputblock-0",
    errorText: "Enter valid name.",
    regex: "^([- a-zA-Z\u00c0-\u024f]+)$"
  };
  let EmailProps = {
    inputId: "email",
    label: "Email Id:",
    type: "email",
    appendAtId: "step1Card-inputblock-4",
    errorText: "Enter valid email.",
    regex: "[w|.|-]*@w*.[w|.]*"
  };
  let MobileNumberProps = {
    inputId: "phone",
    label: "Mobile Number:",
    type: "numbers",
    appendAtId: "step1Card-inputblock-3",
    inputPrefix: "+91",
    errorText: "Enter valid phone number.",
    regex: "^[1-9]{1}[0-9]{9}$"
  };

  let GenderInputProps = {
    label: "Gender:",
    name: "gender",
    type: "radio",
    appendAtId: "step1Card-inputblock-2",
    commonClassForSection: "genderInput",
    inputId: "genderValue",
    options: {
      Male: {
        checked: true,
        image: {
          classList: ["male-input-image", "gender-image", "radioCard"]
        }
      },
      Female: {
        image: {
          classList: ["female-input-image", "gender-image", "radioCard"]
        }
      }
    }
  };
  let mindate = new Date();
  mindate.setFullYear(mindate.getFullYear() - 20);

  let maxDate = new Date();
  maxDate.setFullYear(mindate.getFullYear() - 70);

  let AgeSliderProps = {
    label: "Age :",
    type: "date",
    errorText: "Enter valid date.",
    minDate: `${mindate.getFullYear()}-${mindate.getMonth() +
      1}-${mindate.getDate()}`,
    maxDate: `${maxDate.getFullYear()}-${maxDate.getMonth() +
      1}-${maxDate.getDate()}`,
    appendAtId: "step1Card-inputblock-1",
    suffixValue: {
      text: "years old",
      classList: ["rangeInputPrefixSuffix"]
    },
    inputId: "age"
  };

  let saveButtonProps = {
    appendAtId: "step1Card-inputblock-5",
    inputId: "step1SaveBtn",
    buttonText: "Save",
    classList: ["displayNone", "prevBtn"],
    buttonSuffixClass: ["saveImg"],
    func: saveForm
  };

  let nextButtonProps = {
    appendAtId: "step1Card-inputblock-6",
    inputId: "step1NextBtn",
    buttonText: "Next",
    classList: ["nextBtn"],
    buttonSuffixClass: ["nextImg"],
    func: validateForm
  };

  // props for share wrapper
  // let shareWrapperProps = {
  //   appendAtId: "step1Card-inputblock-7",
  //   inputId: "shareWrapper",
  //   classList: ["shareWrapper"],
  //   heading: {
  //     classList: ["shareWrapper-heading"]
  //   },
  //   linkWrapper: {
  //     classList: ["linkWrapper"]
  //   },
  //   link: {
  //     text: "http://127.0.0.1:8002/ulip-calculator",
  //     classList: ["link"]
  //   },
  //   copyButton: {
  //     classList: ["copyButton"]
  //   }
  // };
  card1 = {
    card: getComponent(cardjs, cardProps),
    NameInput: getComponent(genericInputjs, NameProps),
    EmailInput: getComponent(genericInputjs, EmailProps),
    MobileInput: getComponent(genericInputjs, MobileNumberProps),
    GenderInput: getComponent(genericRadioCardjs, GenderInputProps),
    AgeSlider: getComponent(genericInputjs, AgeSliderProps),
    SaveBtn: getComponent(buttonjs, saveButtonProps),
    NextBtn: getComponent(buttonjs, nextButtonProps)
    // shareWrapper: getComponent(shareWrapperJs, shareWrapperProps) adding component for shareWrapper
  };
}

function stepTwoCardRender() {
  function saveForm(
    fields = {
      date_of_joining: document.getElementById("policyStartDate").innerText,
      life_insurance_cover_type: document.getElementById("typeOfLicValue")
        .innerText,
      tenure_of_policy: parseInt(
        document.getElementById("policyTenureValue").innerText.slice(0, 2)
      ),
      payment_frequency: document.getElementById("contributionFrequencyValue")
        .innerText,
      independent_income: document.getElementById("independentIncomeValue")
        .innerText
    }
  ) {
    prevStep = 2;
    step = 3;
    if (
      fields.date_of_joining &&
      fields.life_insurance_cover_type &&
      fields.tenure_of_policy &&
      fields.payment_frequency &&
      fields.independent_income
    ) {
      Object.keys(fields).map(e => {
        params[e] = fields[e];
      });
    }

    //rendering info for step 2
    let props1 = {
      appendAtId: "step2InfoWrapper",
      infoObject: {
        "Date of joining": params.date_of_joining,
        "Tenure of policy": `${params.tenure_of_policy} years`,
        "Type of life insurance cover": params.life_insurance_cover_type,
        "Contributing payment frequency": params.payment_frequency
      },
      editButtonclassList: ["editIcon", "step1EditButton"],
      inputId: "step1Edit"
    };
    getComponent(step2Js, props1);

    hide("step2Card");
    // emptying card 3's wrapper
    let step3CardWrapper = document.getElementById("step3CardWrapper");
    while (step3CardWrapper.firstChild) {
      step3CardWrapper.removeChild(step3CardWrapper.firstChild);
    }
    render((step = 3));

    document.getElementById("step2Icon").classList.remove("activeIcon");
    document.getElementById("step2Icon").classList.add("filledIcon");

    document.getElementById(`step${step}Icon`).classList.remove("filledIcon");
    document.getElementById(`step${step}Icon`).classList.add("activeIcon");
  }
  function validateForm(
    fields = {
      date_of_joining: document.getElementById("policyStartDate").innerText,
      life_insurance_cover_type: document.getElementById("typeOfLicValue")
        .innerText,
      tenure_of_policy: parseInt(
        document.getElementById("policyTenureValue").innerText.slice(0, 2)
      ),
      payment_frequency: document.getElementById("contributionFrequencyValue")
        .innerText,
      independent_income: document.getElementById("independentIncomeValue")
        .innerText
    }
  ) {
    if (
      fields.date_of_joining &&
      fields.life_insurance_cover_type &&
      fields.tenure_of_policy &&
      fields.payment_frequency &&
      fields.independent_income
    ) {
      prevStep = 2;
      step = 3;
      Object.keys(fields).map(e => {
        params[e] = fields[e];
      });
      // functionality for updating dom to info of first view
      hide("step2Card");
      render((step = 3));
      //rendering info for step 2
      let props1 = {
        appendAtId: "step2InfoWrapper",
        infoObject: {
          "Date of joining": params.date_of_joining,
          "Tenure of policy": `${params.tenure_of_policy} years`,
          "Type of life insurance cover": params.life_insurance_cover_type,
          "Contributing payment frequency": params.payment_frequency
        },
        editButtonclassList: ["editIcon", "step1EditButton"],
        inputId: "step1Edit"
      };
      getComponent(step2Js, props1);

      let step3CardWrapper = document.getElementById("step3CardWrapper");
      while (step3CardWrapper.firstChild) {
        step3CardWrapper.removeChild(step3CardWrapper.firstChild);
      }
      // emptying step 2's info wrapper
      let step2InfoWrapper = document.getElementById("step2InfoWrapper");
      while (step2InfoWrapper.firstChild) {
        step2InfoWrapper.removeChild(step2InfoWrapper.firstChild);
      }

      document
        .getElementById("step2Wrapper")
        .classList.remove("borderInactive");
      document.getElementById("step2Wrapper").classList.add("borderActive");

      document.getElementById("step2Icon").classList.remove("activeIcon");
      document.getElementById("step2Icon").classList.add("filledIcon");

      document.getElementById(`step${step}Icon`).classList.remove("filledIcon");
      document.getElementById(`step${step}Icon`).classList.add("activeIcon");

      console.log("You are redirected to next page");
    }
  }
  function stepBack() {
    hide("step2Card");
    show("step1Card");

    prevStep = 2;
    step = 1;

    document
      .getElementById(`step${prevStep}Icon`)
      .classList.remove("activeIcon");
    document.getElementById(`step${prevStep}Icon`).classList.add("filledIcon");

    document.getElementById(`step${step}Icon`).classList.remove("filledIcon");
    document.getElementById(`step${step}Icon`).classList.add("activeIcon");

    step = step - 1;
    // empty the info div
    let step1InfoWrapper = document.getElementById("step1InfoWrapper");
    while (step1InfoWrapper.firstChild) {
      step1InfoWrapper.removeChild(step1InfoWrapper.firstChild);
    }
    // hide next button & show save button
    show("step1NextBtn");
    hide("step1SaveBtn");
  }
  let date = new Date();
  let cardProps = {
    appendAtId: "step2CardWrapper",
    inputId: "step2Card"
  };
  let policyStartDateProps = {
    appendAtId: "step2Card-inputblock-0",
    infoHeading: "Policy start date",
    inputId: "policyStartDate",
    classList: ["inputInfoValue"],
    infoValue: `${date.getDate()}/${("0" + (date.getMonth() + 1)).slice(
      -2
    )}/${date.getFullYear()}`
  };
  let typeofLICProps = {
    label: "Type of Life Insurance cover",
    name: "typeOfLIC",
    type: "radio",
    appendAtId: "step2Card-inputblock-1",
    inputId: "typeOfLicValue",
    sectionId: "typeofLic",
    commonClassForSection: "typeOfLic",
    options: {
      "Declining Term Insurance Cover": {
        checked: true,
        imageHeading: {
          text: "Declining Term Insurance Cover",
          classList: ["radioCardImageHeading"]
        },
        imageDescription: {
          text:
            "Decreasing term insurance is renewable term life insurance with coverage decreasing over the life of the policy at a predetermined rate",
          classList: ["imageDescription"]
        },
        image: {
          classList: ["decliningTermInsuranceImage", "radioCard"]
        }
      },
      "Fixed Term Insurance Cover": {
        imageHeading: {
          text: "Fixed Term Insurance Cover",
          classList: ["radioCardImageHeading"]
        },
        imageDescription: {
          text:
            "Insurance policy that provides coverage for a certain period of time, or a specified 'term' of years",
          classList: ["imageDescription"]
        },
        image: {
          classList: ["fixedTermInsuranceImage", "radioCard"]
        }
      }
    }
  };
  let policyTenureProps = {
    label: "Tenure of Policy",
    name: "policyTenure",
    type: "radio",
    appendAtId: "step2Card-inputblock-2",
    inputId: "policyTenureValue",
    commonClassForSection: "policyTenure",
    sectionId: "policyTenure",
    options: {
      "10 years": {
        checked: true,
        imageHeading: {
          text: "10 years",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      "15 years": {
        imageHeading: {
          text: "15 years",
          classList: ["radioOnlyHeadingCard"]
        }
      }
    }
  };
  let contributionFrequencyProps = {
    label: "Contribution payment frequency",
    name: "contributionFrequency",
    type: "radio",
    appendAtId: "step2Card-inputblock-3",
    inputId: "contributionFrequencyValue",
    commonClassForSection: "contributionFrequency",
    sectionId: "contributionFrequency",
    options: {
      "One Time": {
        checked: true,
        imageHeading: {
          text: "One Time",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      Yearly: {
        imageHeading: {
          text: "Yearly",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      "Half Yearly": {
        imageHeading: {
          text: "Half Yearly",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      Quaterly: {
        imageHeading: {
          text: "Quaterly",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      Monthly: {
        imageHeading: {
          text: "Monthly",
          classList: ["radioOnlyHeadingCard"]
        }
      }
    }
  };
  let independentIncomeProps = {
    label: "Independent Income",
    type: "radio",
    sectionId: "independentIncome",
    name: "independentIncome",
    appendAtId: "step2Card-inputblock-4",
    commonClassForSection: "contributionFrequency",
    inputId: "independentIncomeValue",
    options: {
      yes: {
        checked: true,
        imageHeading: {
          text: "Yes",
          classList: ["radioOnlyHeadingCard"]
        }
      },
      no: {
        imageHeading: {
          text: "No",
          classList: ["radioOnlyHeadingCard"]
        }
      }
    }
  };
  let nextButtonProps = {
    appendAtId: "step2Card-inputblock-5",
    inputId: "step2-nextBtn",
    buttonText: "Next",
    buttonSuffixClass: ["nextImg"],
    classList: ["nextBtn"],
    func: validateForm
  };
  let prevButtonProps = {
    appendAtId: "step2Card-inputblock-6",
    inputId: "step2-prevBtn",
    buttonText: "Previous",
    buttonSuffixClass: ["PreviousImg"],
    classList: ["prevBtn"],
    func: stepBack
  };
  let saveButtonProps = {
    appendAtId: "step2Card-inputblock-7",
    inputId: "step2-saveBtn",
    buttonText: "Save",
    buttonSuffixClass: ["saveImg"],
    classList: ["displayNone", "prevBtn"],
    func: saveForm
  };

  card2 = {
    card: getComponent(cardjs, cardProps),
    infoDate: getComponent(infojs, policyStartDateProps),
    typeofLIC: getComponent(genericRadioCardjs, typeofLICProps),
    policyTenure: getComponent(genericRadioCardjs, policyTenureProps),
    contributionFrequency: getComponent(
      genericRadioCardjs,
      contributionFrequencyProps
    ),
    independentIncome: getComponent(genericRadioCardjs, independentIncomeProps),
    nextBtn: getComponent(buttonjs, nextButtonProps),
    previousBtn: getComponent(buttonjs, prevButtonProps),
    SaveBtn: getComponent(buttonjs, saveButtonProps)
  };
}

function stepThreeCardRender() {
  function validateForm(
    fields = {
      life_cover_amount: parseInt(document.getElementById("assuredSum").value),
      target_amount: parseInt(document.getElementById("assuredSum").value),
      expected_rate_of_return: parseInt(
        document.getElementById("expectedROI").value
      )
    }
  ) {
    if (fields.life_cover_amount && fields.expected_rate_of_return) {
      Object.keys(fields).map(e => {
        params[e] = fields[e];
      });

      UlipApiCall(params);
    }
  }

  function lifeCoverValue(dob, IndependentIncome, targetAmount, gender) {
    let dateToCalculate = new Date();
    function calculateAge(dateOfBirth, dateToCalculate) {
      dateOfBirth = new Date(dateOfBirth);
      var calculateYear = dateToCalculate.getFullYear();
      var calculateMonth = dateToCalculate.getMonth();
      var calculateDay = dateToCalculate.getDate();

      var birthYear = dateOfBirth.getFullYear();
      var birthMonth = dateOfBirth.getMonth();
      var birthDay = dateOfBirth.getDate();

      var age = calculateYear - birthYear;
      var ageMonth = calculateMonth - birthMonth;
      var ageDay = calculateDay - birthDay;

      if (ageMonth < 0 || (ageMonth == 0 && ageDay < 0)) {
        age = parseInt(age) - 1;
      }
      return age;
    }

    let age = calculateAge(dob, dateToCalculate);

    let result = 0;
    if (age < 18) {
      if (IndependentIncome == 1) {
        if (targetAmount > 500000) result = 500000;
        else result = targetAmount;
      } else result = 0;
    } else {
      if (gender.toLowerCase() == "male") {
        if (targetAmount > 1500000) result = 1500000;
        else result = targetAmount;
      } else {
        if (IndependentIncome == 1) {
          if (targetAmount > 1500000) result = 1500000;
          else result = targetAmount;
        } else {
          if (targetAmount > 500000) result = 500000;
          else result = targetAmount;
        }
      }
    }
    return result;
  }

  let cardProps = {
    appendAtId: "step3CardWrapper",
    inputId: "step3Card"
  };
  let lifeCoverAmountProps = {
    appendAtId: "step3Card-inputblock-1",
    infoHeading: "Life cover amount:",
    inputId: "lifeCoverAmount",
    infoValue: lifeCoverValue(
      params.dob,
      (independentIncome = params.independent_income == "yes" ? 1 : 0),
      100000,
      params.gender
    ),
    classList: []
  };
  let assuredSumProps = {
    label: "Sum assured",
    appendAtId: "step3Card-inputblock-0",
    minValue: 60000,
    maxValue: 1500000,
    defaultValue: 100000,
    funcParameter: {
      dob: params.dob,
      IndependentIncome: params.independent_income == "yes" ? 1 : 0,
      targetAmount: 100000,
      gender: params.gender
    },
    func: lifeCoverValue,
    prefixValue: {
      text: "₹",
      classList: []
    },
    changeValueAt: lifeCoverAmountProps.inputId,
    inputId: "assuredSum",
    step: 100000,
    KRepresentation: true
  };
  let expectedROIProps = {
    label: "Expected rate of return",
    appendAtId: "step3Card-inputblock-2",
    minValue: 5,
    maxValue: 50,
    defaultValue: 10,
    inputId: "expectedROI",
    step: 5,
    suffixValue: {
      text: "%",
      classList: []
    }
  };
  let calculateButtonProps = {
    appendAtId: "step3Card-inputblock-3",
    inputId: "calculateButton",
    buttonText: "Calculate",
    classList: ["calculateButton"],
    func: validateForm
  };

  let card3 = {
    card: getComponent(cardjs, cardProps),
    assuredSum: getComponent(sliderjs, assuredSumProps),
    lifeCoverAmount: getComponent(infojs, lifeCoverAmountProps),
    expectedROI: getComponent(sliderjs, expectedROIProps),
    calculateButton: getComponent(buttonjs, calculateButtonProps)
  };
}

function stepFourCardRender() {
  clearDivison("step1Wrapper");
  clearDivison("step2Wrapper");
  clearDivison("step3Wrapper");
  hide("step1Wrapper");
  hide("step2Wrapper");
  hide("step3Wrapper");

  let cardProps = {
    appendAtId: "step4CardWrapper",
    inputId: "step4Card",
    fullWidth: true
  };
  // calculatedData = {"total_units":10152.732201646146,"date_of_joining":"26/06/2019","maturity_age":" 35 Years 3 Months","expected_rate_of_return":10.0,"message":"Success","total_contribution":100000.0,"maturity_date_of_policy":"26/06/2029","mobile_number":0,"email_id":null,"reference_id":2987,"target_amount":100000.0,"name":"Chavada Hirva","payment_frequency":"One Time","total_mortality_charge":505.0,"life_cover_amount":100000.0,"report_url":"https://indiummf.robosoftin.com/FileServer/Ulip/2987_636971812798199439.pdf","total_nav":26.42143819695023,"tenure_of_policy":10,"gender":"Female","maturity_amount":268250.0,"age":25,"maturity_bonus":5000.0};
  finalReportWrapperProps = {
    appendAtId: "step4Card",
    name: calculatedData.name,
    gender: calculatedData.gender,
    age: calculatedData.age,
    currentExpenses: "100000",
    persentCorpus: "25000000",
    lifeCoverAmount: calculatedData.life_cover_amount,
    maturityBonus: calculatedData.maturity_bonus,
    maturityAmount: calculatedData.maturity_amount,
    dateOfJoining: calculatedData.date_of_joining,
    ageOnJoining: calculatedData.age,
    dateOfMaturity: calculatedData.maturity_date_of_policy,
    ageOnMaturity: calculatedData.maturity_age,
    paymentFrequency: calculatedData.payment_frequency,
    contributionAmount: calculatedData.total_contribution,
    targetAmount: calculatedData.target_amount,
    tenureOfPolicy: calculatedData.tenure_of_policy,
    expectedROR: calculatedData.expected_rate_of_return,
    totalMortalityCharge: calculatedData.total_mortality_charge,
    report_url: calculatedData.report_url
  };
  card4 = {
    card: getComponent(cardjs, cardProps),
    finalReportWrapper: getComponent(
      finalReportWrapperJs,
      finalReportWrapperProps
    )
  };
}

function init() {
  addStyleSheet();
  stepsWrapper();
}

init();
