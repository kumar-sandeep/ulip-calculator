export default props => {
  let shareWrapper = document.createElement("div");
  props.classList.map(e => {
    shareWrapper.classList.add(e);
  });
  shareWrapper.setAttribute("id", props.inputId);
  document.getElementById(props.appendAtId).appendChild(shareWrapper);

  let heading = document.createElement("p");
  shareWrapper.appendChild(heading);
  props.heading.classList.map(e => {
    heading.classList.add(e);
  });
  heading.innerText = "Share Result";

  let linkWrapper = document.createElement("div");
  props.linkWrapper.classList.map(e => {
    linkWrapper.classList.add(e);
  });
  shareWrapper.appendChild(linkWrapper);

  let link = document.createElement("p");
  linkWrapper.appendChild(link);
  props.link.classList.map(e => {
    link.classList.add(e);
  });
  link.innerText = props.link.text;

  let copyButton = document.createElement("p");
  linkWrapper.appendChild(copyButton);
  props.copyButton.classList.map(e => {
    copyButton.classList.add(e);
  });
  copyButton.innerText = "Copy Link";
};
