export default props => {
  let inputWrapper = document.createElement("div");
  inputWrapper.setAttribute("id", props.sectionId);
  inputWrapper.classList.add("Input-Wrapper");

  let inputHeading = document.createElement("div");
  inputHeading.classList.add("Name");
  inputHeading.innerText = props.label;
  inputWrapper.appendChild(inputHeading);

  let value = document.createElement("span");
  value.classList.add("selected-input-value");
  value.setAttribute("id", props.inputId);
  inputHeading.appendChild(value);

  let RadioCardjs = "../../widget/input/radio-card/radio-card.js";
  let servicesjs = "../../../assets/js/services.js";
  import(servicesjs).then(data => {
    let props1 = {
      inputId: props.inputId,
      appendAtId: props.sectionId,
      name: props.name,
      options: props.options,
      commonClassForSection: props.commonClassForSection,
    };
    if(props.func){
      props1.func = props.func
    }

    data.default.getComponent(RadioCardjs, props1);
  });
  document.getElementById(props.appendAtId).appendChild(inputWrapper);
};
