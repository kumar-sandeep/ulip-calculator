export default props => {
  for (let keys in props.options) {
    // radiobutton's wrapper which hold actual radio button. It need to be hidden.
    let radioButtonLabel = document.createElement("label");
    radioButtonLabel.classList.add("radioButtonLabel");
    radioButtonLabel.classList.add(props.commonClassForSection);
    radioButtonLabel.setAttribute("data-value", `radioInput-${keys}`);
    document.getElementById(props.appendAtId).appendChild(radioButtonLabel);

    let inputOption = document.createElement("input");
    inputOption.setAttribute("type", "radio");
    inputOption.setAttribute("value", keys);
    inputOption.setAttribute("name", props.name);
    inputOption.setAttribute("id", `radioInput-${keys}`);
    props.options[keys].checked;
    radioButtonLabel.appendChild(inputOption);

    // custom radio button which is need to be shown
    let customRadioButton = document.createElement("div");
    customRadioButton.classList.add("customRadioButton");

    let imageWrapper = document.createElement("div");
    imageWrapper.classList.add("imageWrapper");
    customRadioButton.appendChild(imageWrapper);

    let ticker = document.createElement("div");
    ticker.classList.add("ticker");
    ticker.setAttribute("id", `radioInput-${keys}-ticker`);
    if (props.options[keys].checked) {
      inputOption.checked = true;
      ticker.classList.add("selected-ticker");
    }
    imageWrapper.appendChild(ticker);

    if (props.options[keys].image) {
      let image = document.createElement("div");
      props.options[keys].image.classList.map(ele => {
        image.classList.add(ele);
      });
      imageWrapper.appendChild(image);
    }

    if (props.options[keys].imageHeading) {
      let imageHeading = document.createElement("div");
      props.options[keys].imageHeading.classList.map(ele => {
        imageHeading.classList.add(ele);
      });
      imageHeading.innerText = props.options[keys].imageHeading.text;
      imageWrapper.appendChild(imageHeading);
    }

    if (props.options[keys].imageDescription) {
      let imageDescription = document.createElement("p");
      props.options[keys].imageDescription.classList.map(ele => {
        imageDescription.classList.add(ele);
      });
      imageDescription.innerText = props.options[keys].imageDescription.text;
      customRadioButton.appendChild(imageDescription);
    }
    radioButtonLabel.appendChild(customRadioButton);
  }
  document.getElementById(props.inputId).innerText = document.querySelector(
    `input[name=${props.name}]:checked`
  ).value;

  let selectedValue = document.querySelectorAll(
    `.${props.commonClassForSection}`
  );
  selectedValue.forEach(ele => {
    ele.addEventListener("click", e => {
      e.preventDefault();
      document.getElementById(ele.getAttribute("data-value")).checked = true;
      document.getElementById(props.inputId).innerText = document.querySelector(
        `input[name=${props.name}]:checked`
      ).value;

      if (props.inputId == "policyTenureValue") {
        params.tenure_of_policy = parseInt(document.querySelector(`input[name=${props.name}]:checked`).value.slice(0, 2));
        if (props.func) {
          props.func(params);
        }
      } else if (props.inputId == "contributionFrequencyValue") {
        params.payment_frequency = parseInt(document.querySelector(`input[name=${props.name}]:checked`).value.slice(0, 2));
        if (props.func) {
          props.func(params);
        }
      }
      let allInputs = document.querySelectorAll(`input[name=${props.name}]`);
      allInputs.forEach(element => {
        if (element.checked) {
          document
            .getElementById(`${element.getAttribute("id")}-ticker`)
            .classList.add("selected-ticker");
        } else {
          document
            .getElementById(`${element.getAttribute("id")}-ticker`)
            .classList.remove("selected-ticker");
        }
      });
    });
  });
};

/*
    RadioButtonLabel
        InputOption
        CustomRadioButton
            ImageWrapper
                Ticker
                Image
                ImageHeading
            ImageDescription


    props need to be structured like this
      options: {
        male: {
        image: {
            class: ["male-input-image"]
        },
        imageHeading: {
            class: [],
        },
        imageDescription: {
            class: []
        }
        },
        female: {
        image: {
            class: ["female-input-image"]
        },
        imageHeading: {
            class: [],
        },
        imageDescription: {
            class: []
        }
    }
  }
*/
