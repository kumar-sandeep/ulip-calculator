//  Default slider component

export default props => {
  let sliderWrapper = document.createElement("div");
  sliderWrapper.classList.add("sliderWrapper", "Name");
  document.getElementById(props.appendAtId).appendChild(sliderWrapper);

  // label for input
  let label = document.createElement("label");
  label.innerText = props.label;
  sliderWrapper.appendChild(label);
  // label for input

  // slider input starts here
  let slider = document.createElement("div");
  slider.classList.add("rangeInputWrapper");
  sliderWrapper.appendChild(slider);

  let rangeInput = document.createElement("input");
  rangeInput.setAttribute("type", "range");
  rangeInput.setAttribute("min", props.minValue);
  rangeInput.setAttribute("max", props.maxValue);
  rangeInput.classList.add("slider");
  rangeInput.value = props.defaultValue;
  rangeInput.setAttribute("id", props.inputId);
  rangeInput.classList.add("rangeInput");
  slider.appendChild(rangeInput);

  // let thumb = document.createElement("div");
  // thumb.classList.add("thumb");
  // slider.appendChild(thumb);

  let guideDiv = document.createElement("div");
  guideDiv.classList.add("guideDiv");
  slider.appendChild(guideDiv);
  for (
    let scale = props.minValue;
    scale <= props.maxValue;
    scale = scale + props.step
  ) {
    let scaleValue = document.createElement("span");
    if (props.KRepresentation) {
      let text = scale / 1000;
      scaleValue.innerText = `${text}k`;
    } else {
      scaleValue.innerText = scale;
    }
    guideDiv.appendChild(scaleValue);
  }

  // slider input ends here

  // inputValueWrapper starts here with all its sub component
  let inputValueWrapper = document.createElement("div");
  inputValueWrapper.classList.add("inputValueWrapper");
  sliderWrapper.appendChild(inputValueWrapper);

  if (props.prefixValue) {
    let prefix = document.createElement("div");
    prefix.classList.add("rangeInputPrefixSuffix");
    for (let i = 0; i < props.prefixValue.classList.length; i++) {
      prefix.classList.add(props.prefixValue.classList[i]);
    }
    prefix.innerText = props.prefixValue.text;
    inputValueWrapper.appendChild(prefix);
  }

  let inputValue = document.createElement("div");
  inputValue.innerText = props.defaultValue;
  inputValue.setAttribute("id", `${props.inputId}-value`);
  inputValue.classList.add("rangeInputValue");
  inputValueWrapper.appendChild(inputValue);

  if (props.suffixValue) {
    let suffix = document.createElement("div");
    for (let i = 0; i < props.suffixValue.classList.length; i++) {
      suffix.classList.add(props.suffixValue.classList[i]);
    }
    suffix.innerText = props.suffixValue.text;
    inputValueWrapper.appendChild(suffix);
  }
  // inputValueWrapper ends here with all its sub component within it

  // functionality to handle changing input
  document.getElementById(props.inputId).addEventListener("change", e => {
    document.getElementById(`${props.inputId}-value`).innerText =
      e.currentTarget.value;
    if (props.inputId == "expectedRate") {
      params.expected_rate_of_return = e.currentTarget.value;
    } else if (props.inputId == "totalWrapper") {
      params.target_amount = e.currentTarget.value;
    }
    if (props.func) {
      props.func(params);
    }
    if (props.changeValueAt) {
      document.getElementById(props.changeValueAt).innerText = props.func(
        props.funcParameter.dob,
        props.funcParameter.IndependentIncome,
        e.currentTarget.value,
        props.funcParameter.gender
      );
    }
  });
};
