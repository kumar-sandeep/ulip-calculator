// exporiting default normal-input components

export default props => {
  props.label ? "" : (props.label = "default");
  props.type ? "" : (props.type = "text");
  props.regex
    ? (props.regex = new RegExp(props.regex))
    : (props.regex = new RegExp("^([- a-zA-Z\u00c0-\u024f]+)$"));

  // wrapper div
  let inputwrapper = document.createElement("div");
  inputwrapper.classList.add("Input-Wrapper", "Name");

  let errorInput = document.createElement("div");
  errorInput.style.width = "50%";

  // input element
  let normalInput = document.createElement("input");
  normalInput.setAttribute("id", props.inputId);
  normalInput.setAttribute("type", props.type);
  normalInput.classList.add("Input");
  if (props.minDate) {
    normalInput.setAttribute("min", props.minDate);
  }
  if (props.maxDate) {
    normalInput.setAttribute("max", props.maxDate);
  }
  errorInput.appendChild(normalInput);

  let errorDiv = document.createElement("div");
  errorDiv.setAttribute("id", `${props.inputId}-error`);
  errorDiv.classList.add("error-display");
  errorInput.appendChild(errorDiv);

  // input's label
  let normalLabel = document.createElement("label");
  normalLabel.classList.add("Name");
  normalLabel.style.display = "flex";
  normalLabel.innerText = props.label;

  //input prefix span
  if (props.inputPrefix) {
    let inputPrefix = document.createElement("span");
    inputPrefix.innerText = props.inputPrefix;
    inputPrefix.style.marginLeft = "10px";
    normalLabel.append(inputPrefix);
  }

  normalLabel.append(errorInput);
  inputwrapper.append(normalLabel);

  // return inputwrapper
  document.getElementById(props.appendAtId).appendChild(inputwrapper);

  normalInput.addEventListener("keyup", e => {
    let regex = props.regex;
    let val = e.currentTarget.value;
    let errorText;
    if (props.type == "date") {
      let date = new Date();
      let inputValue = new Date(e.currentTarget.value);
      inputValue < date ? (errorText = "") : (errorText = props.errorText);
      errorDiv.innerText = errorText;
    } else {
      regex.test(val) ? (errorText = "") : (errorText = props.errorText);
      errorDiv.innerText = errorText;
    }
  });
};
