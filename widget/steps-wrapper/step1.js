export default props => {
  let step1Info = document.createElement("div");
  step1Info.setAttribute("id", "step1Info");
  step1Info.classList.add("step1Info");
  document.getElementById(props.appendAtId).appendChild(step1Info);

  // edit icon for info
  let editIcon = document.createElement("div");
  step1Info.appendChild(editIcon);
  editIcon.setAttribute("id", props.inputId);
  props.editButtonclassList.map(e => {
    editIcon.classList.add(e);
  });

  //Info to be shown
  Object.keys(props.infoObject).map(e => {
    let info = document.createElement("label");
    step1Info.appendChild(info);

    if (e == "br") {
      let sepratorDiv = document.createElement("div");
      sepratorDiv.classList.add("horoizontalSeprator");
      step1Info.appendChild(sepratorDiv);
    } else {
      let spanLabel = document.createElement("span");
      info.appendChild(spanLabel);
      spanLabel.innerText = `${e}:`;

      let spanValue = document.createElement("span");
      spanValue.classList.add("bold");
      info.appendChild(spanValue);
      spanValue.innerText = `${props.infoObject[e]}, `;
    }
  });

  editIcon.addEventListener("click", e => {
    let step1InfoWrapper = document.getElementById("step1InfoWrapper");
    while (step1InfoWrapper.firstChild) {
      step1InfoWrapper.removeChild(step1InfoWrapper.firstChild);
    }
    let step3CardWrapper = document.getElementById("step3CardWrapper");
    while (step3CardWrapper.firstChild) {
      step3CardWrapper.removeChild(step3CardWrapper.firstChild);
    }
    hide("step2Card");
    hide("step1NextBtn");
    show("step1SaveBtn");
    show("step1Card");
    document.getElementById("step1Icon").classList.remove("filledIcon");
    document.getElementById("step1Icon").classList.add("activeIcon");
    prevStep = step;
    step=prevStep
    console.log(prevStep);
    document.getElementById(`step${prevStep}Icon`).classList.remove("activeIcon")
    document.getElementById(`step${prevStep}Icon`).classList.add("filledIcon")
  });
};
