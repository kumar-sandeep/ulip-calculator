export default props => {
  let wrapper = document.createElement("div");
  wrapper.setAttribute("id", "container-wrapper");
  document.getElementById(props.appendAtId).appendChild(wrapper);

  // stepWrapper divison
  let stepsWrapper = document.createElement("div");
  wrapper.appendChild(stepsWrapper);

  // step one wrapper
  let step1Wrapper = document.createElement("div");
  step1Wrapper.setAttribute("id", "step1Wrapper");
  step1Wrapper.classList.add("borderInactive");
  stepsWrapper.appendChild(step1Wrapper);

  // icon for progress bar
  let step1Icon = document.createElement("div");
  step1Icon.setAttribute("id", "step1Icon");
  step1Icon.classList.add("activeIcon");
  step1Wrapper.appendChild(step1Icon);

  // step one info wrapper
  let step1InfoWrapper = document.createElement("div");
  step1InfoWrapper.setAttribute("id", "step1InfoWrapper");
  step1Wrapper.appendChild(step1InfoWrapper);

  // step one Card Wrapper
  let step1CardWrapper = document.createElement("div");
  step1CardWrapper.setAttribute("id", "step1CardWrapper");
  step1Wrapper.appendChild(step1CardWrapper);

  // step two wrapper
  let step2Wrapper = document.createElement("div");
  step2Wrapper.classList.add("borderInactive");
  step2Wrapper.setAttribute("id", "step2Wrapper");
  stepsWrapper.appendChild(step2Wrapper);

  // icon for progress bar
  let step2Icon = document.createElement("div");
  step2Icon.setAttribute("id", "step2Icon");
  step2Icon.classList.add("inactiveIcon");
  step2Wrapper.appendChild(step2Icon);

  // step two info wrapper
  let step2InfoWrapper = document.createElement("div");
  step2Wrapper.appendChild(step2InfoWrapper);
  step2InfoWrapper.setAttribute("id", "step2InfoWrapper");

  // step two Card Wrapper
  let step2CardWrapper = document.createElement("div");
  step2CardWrapper.setAttribute("id", "step2CardWrapper");
  step2Wrapper.appendChild(step2CardWrapper);

  // step three wrapper
  let step3Wrapper = document.createElement("div");
  step3Wrapper.setAttribute("id", "step3Wrapper");
  stepsWrapper.appendChild(step3Wrapper);

  // icon for progress bar
  let step3Icon = document.createElement("div");
  step3Icon.setAttribute("id", "step3Icon");
  step3Icon.classList.add("inactiveIcon");
  step3Wrapper.appendChild(step3Icon);

  // step three Card Wrapper
  let step3CardWrapper = document.createElement("div");
  step3CardWrapper.setAttribute("id", "step3CardWrapper");
  step3Wrapper.appendChild(step3CardWrapper);

  // step four wrapper
  let step4Wrapper = document.createElement("div");
  step4Wrapper.setAttribute("id", "step4Wrapper");
  stepsWrapper.appendChild(step4Wrapper);

  // step four Card Wrapper
  let step4CardWrapper = document.createElement("div");
  step4CardWrapper.setAttribute("id", "step4CardWrapper");
  step4Wrapper.appendChild(step4CardWrapper);

  if (props.func) {
    props.func();
  }
};
