export default props => {
  let step2Info = document.createElement("div");
  step2Info.setAttribute("id", "step2Info");
  step2Info.classList.add("step1Info");
  document.getElementById(props.appendAtId).appendChild(step2Info);

  // edit icon for info
  let edit2Icon = document.createElement("div");
  step2Info.appendChild(edit2Icon);
  edit2Icon.setAttribute("id", props.inputId);
  props.editButtonclassList.map(e => {
    edit2Icon.classList.add(e);
  });

  //Info to be shown
  Object.keys(props.infoObject).map(e => {
    let info = document.createElement("label");
    step2Info.appendChild(info);

    if (e == "br") {
      let sepratorDiv = document.createElement("div");
      sepratorDiv.classList.add("horoizontalSeprator");
      step2Info.append(sepratorDiv);
    } else {
      let spanLabel = document.createElement("span");
      info.appendChild(spanLabel);
      spanLabel.innerText = `${e}:`;

      let spanValue = document.createElement("span");
      spanValue.classList.add("bold");
      info.appendChild(spanValue);
      spanValue.innerText = `${props.infoObject[e]}, `;
    }
  });

  edit2Icon.addEventListener("click", e => {
    let step2InfoWrapper = document.getElementById("step2InfoWrapper");
    while (step2InfoWrapper.firstChild) {
      step2InfoWrapper.removeChild(step2InfoWrapper.firstChild);
    }
    hide("step3Card");
    hide("step2-prevBtn");
    hide("step2-nextBtn");
    show("step2-saveBtn");
    show("step2Card");
    prevStep = step;
    step=prevStep
    document.getElementById("step2Icon").classList.remove("filledIcon");
    document.getElementById("step2Icon").classList.add("activeIcon");
    document.getElementById(`step${prevStep}Icon`).classList.remove("activeIcon")
    document.getElementById(`step${prevStep}Icon`).classList.add("filledIcon")
  });
};
