export default props => {
  let infoWrapper = document.createElement("div");
  infoWrapper.classList.add("Input-Wrapper", "inputInfo");
  document.getElementById(props.appendAtId).appendChild(infoWrapper);

  let infoHeading = document.createElement("p");
  infoHeading.innerText = props.infoHeading;
  infoHeading.classList.add("Name");
  infoWrapper.appendChild(infoHeading);

  let infoValue = document.createElement("p");
  infoValue.innerText = props.infoValue;
  for (let i = 0; i < props.classList.length; i++) {
    infoValue.classList.add(props.classList[i]);
  }
  infoValue.setAttribute("id", props.inputId);
  infoWrapper.appendChild(infoValue);
};
