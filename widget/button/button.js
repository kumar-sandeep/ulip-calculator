export default props => {
  let button = document.createElement("button");
  button.setAttribute("id", props.inputId);
  button.setAttribute("type", "button");
  for(let i=0; i<props.classList.length; i++) {
   button.classList.add(props.classList[i]);
  }
  document.getElementById(props.appendAtId).appendChild(button);

  if (props.buttonPrefixClass) {
    // wrapper for adding prefix image
    let buttonPrefixImage = document.createElement("div");
    for (let i = 0; i <= props.buttonPrefixClass.length; i++) {
      button.classList.add(props.buttonPrefixClass[i]);
    }
    button.appendChild(buttonPrefixImage);
  }

  let buttonText = document.createElement("span");

  buttonText.classList.add("buttonText");
  
  buttonText.innerText = props.buttonText;
  if(props.buttonText1 === 'Download Result' || props.buttonText1 === 'Share Result'){
    buttonText.classList.remove("buttonText");
    buttonText.innerText = props.buttonText1;
  }
  button.appendChild(buttonText);

  if (props.buttonSuffixClass) {
    // wrapper for adding suffix image
    let buttonSuffixImage = document.createElement("div");
    for (let i = 0; i <= props.buttonSuffixClass.length; i++) {
      buttonSuffixImage.classList.add(props.buttonSuffixClass[i]);
    }
    button.appendChild(buttonSuffixImage);
  }

  button.addEventListener("click", () => {
    console.log("clicked");
    props.func();
  });
};