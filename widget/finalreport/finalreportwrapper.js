export default props => {
  // common component path
  let genericRadioCardjs = "../../widget/input/radio-card/generic-radio-wrapper.js";
  let sliderjs = "../../widget/input/slider/slider.js";
  let buttonjs = "../../widget/button/button.js";
  let servicesjs = "../../../assets/js/services.js";

  // full container
  let finalReportWrapper = document.createElement("div");
  finalReportWrapper.setAttribute("id", "finalReportWrapper");
  finalReportWrapper.classList.add("flex", "row");
  document.getElementById(props.appendAtId).appendChild(finalReportWrapper)

  let leftWrapper = document.createElement("div");
  leftWrapper.setAttribute("id", "leftWrapper");
  leftWrapper.classList.add("flex", "column");
  finalReportWrapper.appendChild(leftWrapper);

  // Name Title 
  let nameHeadingWrapper = document.createElement("div");
  let nameHeading = document.createElement("div");
  nameHeading.classList.add("nameTitle");
  nameHeading.innerText = props.name.toUpperCase() +" ("+ props.gender.toUpperCase() + ", "+ props.age +" YEARS)";
  nameHeadingWrapper.appendChild(nameHeading);
  leftWrapper.appendChild(nameHeadingWrapper);

  // Tenture of Policy - Radio Button
  let policyTenureWrapper = document.createElement("div");
  policyTenureWrapper.classList.add("pad-b-10");
  policyTenureWrapper.setAttribute("id", "policyTenureWrapper");
  leftWrapper.appendChild(policyTenureWrapper);
  

  import(servicesjs).then(data => {
    let policyTenureProps = {
      label: "Tenure of Policy",
      name: "policyTenure",
      type: "radio",
      appendAtId: "policyTenureWrapper",
      inputId: "policyTenureValue",
      commonClassForSection: "policyTenureWrapper",
      sectionId: "policyTenure2",
      func: UlipApiCall,
      options: {
        "10 years": {
          checked: params.tenure_of_policy == 10 ? true : false,
          imageHeading: {
            text: "10 years",
            classList: ["radioOnlyHeadingCard"]
          }
        },
        "15 years": {
          checked: params.tenure_of_policy == 15 ? true : false,
          imageHeading: {
            text: "15 years",
            classList: ["radioOnlyHeadingCard"]
          }
        }
      }
    };

    data.default.getComponent(genericRadioCardjs, policyTenureProps);
  });

  // Contribution payment frequency - Radio Button
  let contributionWrapper = document.createElement("div");
  contributionWrapper.classList.add("pad-b-10");
  contributionWrapper.setAttribute("id", "contributionWrapper");
  leftWrapper.appendChild(contributionWrapper);
  
  import(servicesjs).then(data => {
    let contributionFrequencyProps = {
      label: "Contribution payment frequency",
      name: "contributionFrequency",
      type: "radio",
      appendAtId: "contributionWrapper",
      inputId: "contributionFrequencyValue",
      commonClassForSection: "contributionWrapper",
      sectionId: "contributionFrequency1",
      func: UlipApiCall,
      options: {
        "One Time": {
          checked: true,
          imageHeading: {
            text: "One Time",
            classList: ["radioOnlyHeadingCard"]
          }
        },
        Yearly: {
          imageHeading: {
            text: "Yearly",
            classList: ["radioOnlyHeadingCard"]
          }
        },
        "Half Yearly": {
          imageHeading: {
            text: "Half Yearly",
            classList: ["radioOnlyHeadingCard"]
          }
        },
        Quaterly: {
          imageHeading: {
            text: "Quaterly",
            classList: ["radioOnlyHeadingCard"]
          }
        },
        Monthly: {
          imageHeading: {
            text: "Monthly",
            classList: ["radioOnlyHeadingCard"]
          }
        }
      }
    };

    data.default.getComponent(genericRadioCardjs, contributionFrequencyProps);
  });

  // Slider for Expected Rate
  let expectedRateWrapper = document.createElement("div");
  expectedRateWrapper.setAttribute("id", "expectedRateWrapper");
  expectedRateWrapper.classList.add("pad-b-30");
  leftWrapper.appendChild(expectedRateWrapper);
  
  import(servicesjs).then(data => {
    let expectedRateProps = {
      label: "Expected Rate of Return ( in % )",
      appendAtId: "expectedRateWrapper",
      minValue: 5,
      maxValue: 50,
      defaultValue: 10,
      func: UlipApiCall,
      suffixValue: {
        text: "%",
        classList: []
      },
      inputId: "expectedRate",
      step: 15,
      KRepresentation: false
    };
    getComponent(sliderjs, expectedRateProps);
  });

  // Slider for Total Contribution / Target Amount
  let totalContributionWrapper = document.createElement("div");
  totalContributionWrapper.setAttribute("id", "totalContributionWrapper");
  totalContributionWrapper.classList.add("pad-b-30");
  leftWrapper.appendChild(totalContributionWrapper);
  
  import(servicesjs).then(data => {
    let totalContributionProps = {
      label: "Total Contribution / Target Amount",
      appendAtId: "totalContributionWrapper",
      minValue: 60000,
      maxValue: 150000,
      defaultValue: 70000,
      func: UlipApiCall,
      prefixValue: {
        text: "₹",
        classList: []
      },
      inputId: "totalWrapper",
      step: 10000,
      KRepresentation: true
    };
    getComponent(sliderjs, totalContributionProps);
  });

  let rightWrapper = document.createElement("div");
  rightWrapper.setAttribute("id", "rightWrapper");
  // flex column , border grey
  rightWrapper.classList.add("flex", "column", "round-border", "space-between");
  finalReportWrapper.appendChild(rightWrapper);

  // let firstRow = document.createElement("div");
  // // firstRow.setAttribute("id", "firstRow");
  // // flex row , border grey
  // firstRow.classList.add("flex", "row");
  // rightWrapper.appendChild(firstRow);

  // let expensesTitleWrapper = document.createElement("div");
  // // expensesTitleWrapper.classList.add("text-center");
  // let expensesTitle = document.createElement("div");
  // expensesTitle.classList.add("amountTitle");
  // expensesTitle.innerHTML = "With the current Expenses of ₹"+ props.currentExpenses.bold() + " and present Corpus of ₹"+ props.persentCorpus.bold();
  // expensesTitleWrapper.appendChild(expensesTitle);
  // firstRow.appendChild(expensesTitleWrapper);

  let secondRow = document.createElement("div");
  secondRow.setAttribute("id", "secondRow");
  // flex row , border grey
  secondRow.classList.add("flex", "row", "space-between", "text-center", "border-bottom");
  rightWrapper.appendChild(secondRow);

  let r2c1 = document.createElement("div");
  r2c1.classList.add("flex", "column");
  let lcaTitle = document.createElement("div");
  lcaTitle.classList.add("amount-title");
  lcaTitle.innerText = "Life Cover Amount:";
  r2c1.appendChild(lcaTitle);

  let lcaAmount = document.createElement("div");
  lcaAmount.classList.add("amount", "blue-text");
  
  let lcaAmountSpan = document.createElement("span");
  lcaAmountSpan.classList.add("indianRupee",  "blue-text");
  lcaAmountSpan.innerText = "₹";
  lcaAmount.appendChild(lcaAmountSpan);


  let lcaAmountSpan1 = document.createElement("span");
  // lcaAmountSpan1.classList.add("indianRupee");
  lcaAmountSpan1.innerHTML = props.lifeCoverAmount;
  lcaAmount.appendChild(lcaAmountSpan1);
  
  r2c1.appendChild(lcaAmount);
  secondRow.appendChild(r2c1);

  let r2c1d1 = document.createElement("div");
  r2c1d1.classList.add("divider");
  secondRow.appendChild(r2c1d1);

  let r2c2 = document.createElement("div");
  r2c2.classList.add("flex", "column");
  let mbTitle = document.createElement("div");
  mbTitle.classList.add("amount-title");
  mbTitle.innerText = "Maturity Bonus:";
  r2c2.appendChild(mbTitle);

  let mbAmount = document.createElement("div");
  mbAmount.classList.add("amount", "green-text");

  let mbAmountSpan = document.createElement("span");
  mbAmountSpan.classList.add("indianRupee",  "green-text");
  // mbAmountSpan.innerText = "₹";
  mbAmountSpan.innerHTML = "₹";
  mbAmount.appendChild(mbAmountSpan);
  
  let mbAmountSpan1 = document.createElement("span");
  mbAmountSpan1.classList.add("indianRupee",  "green-text");
  mbAmountSpan1.innerText = props.maturityBonus;
  mbAmount.appendChild(mbAmountSpan1);

  // mbAmount.innerText = props.maturityBonus;
  r2c2.appendChild(mbAmount);
  secondRow.appendChild(r2c2);

  let r2c2d1 = document.createElement("div");
  r2c2d1.classList.add("divider");
  secondRow.appendChild(r2c2d1);
  
  let r2c3 = document.createElement("div");
  r2c3.classList.add("flex", "column");
  let maTitle = document.createElement("div");
  maTitle.classList.add("amount-title");
  maTitle.innerText = "Maturity Amount";
  r2c3.appendChild(maTitle);

  let maAmount = document.createElement("div");
  maAmount.classList.add("amount", "orange-text");

  let maAmountSpan = document.createElement("span");
  maAmountSpan.classList.add("indianRupee", "orange-text");
  maAmountSpan.innerText = "₹";
  maAmount.appendChild(maAmountSpan);

  let maAmountSpan1 = document.createElement("span");
  maAmountSpan1.classList.add("indianRupee", "orange-text");
  maAmountSpan1.innerText = props.maturityAmount;
  maAmount.appendChild(maAmountSpan1);

  // maAmount.innerText = props.maturityAmount;
  r2c3.appendChild(maAmount);
  secondRow.appendChild(r2c3);

  // third row
  let thirdRow = document.createElement("div");
  thirdRow.setAttribute("id", "thirdRow");
  // flex row , border grey
  thirdRow.classList.add("flex", "row", "space-between");
  rightWrapper.appendChild(thirdRow);

  let r3c1 = document.createElement("div");
  r3c1.classList.add("flex", "column", "col-1");
  let r3c1r1 = document.createElement("div");
  r3c1r1.classList.add("flex", "row", "pad-b-10");
  
  let dojTitle = document.createElement("span");
  dojTitle.classList.add("rowTitle");
  dojTitle.innerText = "Date of Joining:";
  r3c1r1.appendChild(dojTitle);

  let dojAmount = document.createElement("span");
  dojAmount.classList.add("rowValue");
  dojAmount.innerText = props.dateOfJoining;
  r3c1r1.appendChild(dojAmount);

  r3c1.appendChild(r3c1r1);

  let r3c1r2 = document.createElement("div");
  r3c1r2.classList.add("flex", "row", "pad-b-10");
  
  let aojTitle = document.createElement("span");
  aojTitle.classList.add("rowTitle");
  aojTitle.innerText = "Age on Joining:";
  r3c1r2.appendChild(aojTitle);

  let aojAmount = document.createElement("span");
  aojAmount.classList.add("rowValue");
  aojAmount.innerText = props.ageOnJoining+" Years";
  r3c1r2.appendChild(aojAmount);

  r3c1.appendChild(r3c1r2);
  thirdRow.appendChild(r3c1);

  let r3c2 = document.createElement("div");
  r3c2.classList.add("flex", "column", "half-width");
  let r3c2r1 = document.createElement("div");
  r3c2r1.classList.add("flex", "row", "pad-b-10");
  
  let mjTitle = document.createElement("span");
  mjTitle.classList.add("rowTitle");
  mjTitle.innerText = "Maturity date:";
  r3c2r1.appendChild(mjTitle);

  let mjAmount = document.createElement("span");
  mjAmount.classList.add("rowValue");
  mjAmount.innerText = props.dateOfMaturity;
  r3c2r1.appendChild(mjAmount);

  r3c2.appendChild(r3c2r1);

  let r3c2r2 = document.createElement("div");
  r3c2r2.classList.add("flex", "row", "pad-b-10");
  
  let maTitle1 = document.createElement("span");
  maTitle1.classList.add("rowTitle");
  maTitle1.innerText = "Maturity age:";
  r3c2r2.appendChild(maTitle1);

  let maAmount1 = document.createElement("span");
  maAmount1.classList.add("rowValue");
  maAmount1.innerText = props.ageOnMaturity;
  r3c2r2.appendChild(maAmount1);

  r3c2.appendChild(r3c2r2);
  thirdRow.appendChild(r3c2);

  // fourth row
  let fourthRow = document.createElement("div");
  fourthRow.setAttribute("id", "fourthRow");
  // flex row , border grey
  fourthRow.classList.add("flex", "row", "space-between");
  rightWrapper.appendChild(fourthRow);

  let r4c1 = document.createElement("div");
  r4c1.classList.add("flex", "column", "col-1");
  let r4c1r1 = document.createElement("div");
  r4c1r1.classList.add("flex", "row", "pad-b-10");
  
  let pfTitle = document.createElement("span");
  pfTitle.classList.add("rowTitle");
  pfTitle.innerText = "Payment frequency:";
  r4c1r1.appendChild(pfTitle);

  let pfAmount = document.createElement("span");
  pfAmount.classList.add("rowValue");
  pfAmount.innerText = props.paymentFrequency;
  r4c1r1.appendChild(pfAmount);

  r4c1.appendChild(r4c1r1);

  let r4c1r2 = document.createElement("div");
  r4c1r2.classList.add("flex", "row", "pad-b-10");
  
  let cmTitle = document.createElement("span");
  cmTitle.classList.add("rowTitle");
  cmTitle.innerText = "Contribution amount:";
  r4c1r2.appendChild(cmTitle);

  // let cmAmount = document.createElement("span");
  // cmAmount.classList.add("rowValue");
  // cmAmount.innerText = props.contributionAmount;
  // r4c1r2.appendChild(cmAmount);

  let cmAmount = document.createElement("div");
  cmAmount.classList.add("rowValue");
  let cmAmountSpan = document.createElement("span");
  cmAmountSpan.classList.add("indianRupee-small");
  cmAmountSpan.innerText = "₹";
  cmAmount.appendChild(cmAmountSpan);

  let cmAmountSpan1 = document.createElement("span");
  cmAmountSpan1.innerText = props.contributionAmount;
  cmAmount.appendChild(cmAmountSpan1);
  r4c1r2.appendChild(cmAmount);
  r4c1.appendChild(r4c1r2);

  let r4c1r3 = document.createElement("div");
  r4c1r3.classList.add("flex", "row", "pad-b-10");
  
  let taTitle = document.createElement("span");
  taTitle.classList.add("rowTitle");
  taTitle.innerText = "Target amount:";
  r4c1r3.appendChild(taTitle);

  // let taAmount = document.createElement("span");
  // taAmount.classList.add("rowValue");
  // taAmount.innerText = props.targetAmount;
  // r4c1r3.appendChild(taAmount);

  let taAmount = document.createElement("div");
  taAmount.classList.add("rowValue");
  let taAmountSpan = document.createElement("span");
  taAmountSpan.classList.add("indianRupee-small");
  taAmountSpan.innerText = "₹";
  taAmount.appendChild(taAmountSpan);

  let taAmountSpan1 = document.createElement("span");
  taAmountSpan1.innerText = props.targetAmount;
  taAmount.appendChild(taAmountSpan1);
  r4c1r3.appendChild(taAmount);

  r4c1.appendChild(r4c1r3);
  fourthRow.appendChild(r4c1);

  let r4c2 = document.createElement("div");
  r4c2.classList.add("flex", "column", "half-width");
  let r4c2r1 = document.createElement("div");
  r4c2r1.classList.add("flex", "row", "pad-b-10");
  
  let tpTitle = document.createElement("span");
  tpTitle.classList.add("rowTitle");
  tpTitle.innerText = "Tenure of Policy:";
  r4c2r1.appendChild(tpTitle);

  let tpAmount = document.createElement("span");
  tpAmount.classList.add("rowValue");
  tpAmount.innerText = props.tenureOfPolicy;
  r4c2r1.appendChild(tpAmount);

  r4c2.appendChild(r4c2r1);

  let r4c2r2 = document.createElement("div");
  r4c2r2.classList.add("flex", "row", "pad-b-10");
  
  let erTitle = document.createElement("span");
  erTitle.classList.add("rowTitle");
  erTitle.innerText = "Expected rate of return:";
  r4c2r2.appendChild(erTitle);

  let erAmount = document.createElement("span");
  erAmount.classList.add("rowValue");
  erAmount.innerText = props.expectedROR;
  r4c2r2.appendChild(erAmount);

  r4c2.appendChild(r4c2r2);

  let r4c2r3 = document.createElement("div");
  r4c2r3.classList.add("flex", "row", "pad-b-10");
  
  let tmTitle = document.createElement("span");
  tmTitle.classList.add("rowTitle");
  tmTitle.innerText = "Total mortality charge:";
  r4c2r3.appendChild(tmTitle);

  // let tmAmount = document.createElement("span");
  // tmAmount.classList.add("rowValue");
  // tmAmount.innerText = props.totalMortalityCharge;
  // r4c2r3.appendChild(tmAmount);

  let tmAmount = document.createElement("div");
  tmAmount.classList.add("rowValue");
  let tmAmountSpan = document.createElement("span");
  tmAmountSpan.classList.add("indianRupee-small");
  tmAmountSpan.innerText = "₹";
  tmAmount.appendChild(tmAmountSpan);

  let tmAmountSpan1 = document.createElement("span");
  tmAmountSpan1.innerText = props.totalMortalityCharge;
  tmAmount.appendChild(tmAmountSpan1);
  r4c2r3.appendChild(tmAmount);

  r4c2.appendChild(r4c2r3);
  fourthRow.appendChild(r4c2);


  // fourth row
  let fifthRow = document.createElement("div");
  fifthRow.setAttribute("id", "fifthRow");
  // flex row , border grey
  fifthRow.classList.add("flex", "row", "space-around");
  rightWrapper.appendChild(fifthRow);

  let drBtnWraper = document.createElement("div");
  drBtnWraper.classList.add("pad-b-10");
  drBtnWraper.setAttribute("id", "drBtnWraper");
  fifthRow.appendChild(drBtnWraper);

  import(servicesjs).then(data => {
    function drBtnClick() {
      window.open(props.report_url,'_blank');
    }

    let drProps = {
      appendAtId: "drBtnWraper",
      inputId: "drBtn",
      buttonText: "",
      buttonText1: "Download Result",
      classList: ["drBtn"],
      func: drBtnClick
    };

    data.default.getComponent(buttonjs, drProps);
  });
  
  let srBtnWraper = document.createElement("div");
  srBtnWraper.classList.add("pad-b-10");
  srBtnWraper.setAttribute("id", "srBtnWraper");
  fifthRow.appendChild(srBtnWraper);

  import(servicesjs).then(data => {
    function srBtnClick() {
      console.log('Share result Button clicked');
    }

    let drProps = {
      appendAtId: "srBtnWraper",
      inputId: "srBtn",
      buttonText: "",
      buttonText1: "Share Result",
      classList: ["srBtn"],
      func: srBtnClick
    };

    data.default.getComponent(buttonjs, drProps);
  });
  
}