//exporting default card components

export default props => {
  let card = document.createElement("div");
  card.setAttribute("id", props.inputId);
  card.setAttribute("class", "Rectangle");
  if(props.fullWidth) card.classList.add("full-width");
  for(let i=0;i<8;i++){
    let inputBlock = document.createElement('div')
    inputBlock.setAttribute('id',`${props.inputId}-inputblock-${i}`)
    card.appendChild(inputBlock)
  }
  // return card
  document.getElementById(props.appendAtId).appendChild(card);
};